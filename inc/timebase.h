/*
 * timebase.h
 *
 *  Created on: 17.11.2018
 *      Author: alex
 */

#ifndef TIMEBASE_H_
#define TIMEBASE_H_
namespace hal {
enum timebase_t : int { SECONDS = 1, MILLIS = 1000, MICROS = 1000000 };
}

#endif /* TIMEBASE_H_ */
