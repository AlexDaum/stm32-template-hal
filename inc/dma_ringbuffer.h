/*
 * dma_ringbuffer.h
 * A DMA Ringbuffer can be used to read data like from a ringbuffer, but it is
 * filled with a DMA
 *
 *  Created on: 27.12.2018 Author: alex
 */

#ifndef INC_DMA_RINGBUFFER_H_
#define INC_DMA_RINGBUFFER_H_

#include "haldefs.h"
#include "stm.h"
#include <type_traits>

namespace hal {
template <typename t, uintptr_t dma, size_t size> class DMARingBuffer {
  private:
	volatile t buffer[size];
	u32 idx;

	u32 getDMACnt() {
		DMA_Channel_TypeDef *dma_channel =
			reinterpret_cast<DMA_Channel_TypeDef *>(dma);
		return size - dma_channel->CNDTR;
	}

  public:
	DMARingBuffer() : idx(0) {
		static_assert(std::is_integral_v<t>);
		DMA_Channel_TypeDef *dma_channel =
			reinterpret_cast<DMA_Channel_TypeDef *>(dma);

		int msize;
		const size_t typesize = sizeof(t);
		if constexpr (typesize == 8)
			msize = 0;
		else if constexpr (typesize == 16)
			msize = 0b01;
		else
			msize = 0b11;

		dma_channel->CCR &= ~DMA_CCR_EN;
		dma_channel->CNDTR = size;
		dma_channel->CMAR = (u32)buffer;
		dma_channel->CCR |=
			DMA_CCR_CIRC | (msize << DMA_CCR_MSIZE_Pos) | (DMA_CCR_PSIZE);
	}
	~DMARingBuffer() {}

	void startDMA() {
		DMA_Channel_TypeDef *dma_channel =
			reinterpret_cast<DMA_Channel_TypeDef *>(dma);
		dma_channel->CCR |= DMA_CCR_EN;
	}

	t get() {
		if (!hasData())
			return 0;
		t ret = buffer[idx];
		idx++;
		if (idx >= size)
			idx = 0;
		return ret;
	}

	volatile t *getAddrForDMA() { return buffer; }

	bool hasData() { return getDMACnt() != idx; }
};

} // namespace hal

#endif /* INC_DMA_RINGBUFFER_H_ */
