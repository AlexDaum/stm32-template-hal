/*
 * uart_f030_impl.h
 *
 *  Created on: 28.12.2018
 *      Author: alex
 */

#pragma once

#include "clocks.h"
#include "serial.h"

// Serial 1

template <> constexpr uintptr_t hal::getDMAForSerial<USART1_BASE, hal::SEND>() {
	return DMA1_BASE;
}

template <>
constexpr uintptr_t hal::getDMAForSerial<USART1_BASE, hal::RECEIVE>() {
	return DMA1_BASE;
}

template <>
constexpr uintptr_t hal::getDMAChannelForSerial<USART1_BASE, hal::SEND>() {
	return DMA1_Channel4_BASE;
}

template <>
constexpr uintptr_t hal::getDMAChannelForSerial<USART1_BASE, hal::RECEIVE>() {
	return DMA1_Channel5_BASE;
}

template <>
constexpr uintptr_t
hal::getDMAChannelNumberForSerial<USART1_BASE, hal::SEND>() {
	return 4;
}

template <>
constexpr uintptr_t
hal::getDMAChannelNumberForSerial<USART1_BASE, hal::RECEIVE>() {
	return 5;
}

template <> inline void hal::enableIRQs<USART1_BASE>() {
	NVIC_EnableIRQ(USART1_IRQn);
}

#define SERIAL1 USART1_EXTI25_IRQHandler
extern void (*SERIAL1_Handler)();

template <> inline void hal::setIRQ<USART1_BASE>(void (*irq)()) {
	SERIAL1_Handler = irq;
}

// Serial 2

template <> constexpr uintptr_t hal::getDMAForSerial<USART2_BASE, hal::SEND>() {
	return DMA1_BASE;
}

template <>
constexpr uintptr_t hal::getDMAForSerial<USART2_BASE, hal::RECEIVE>() {
	return DMA1_BASE;
}

template <>
constexpr uintptr_t hal::getDMAChannelForSerial<USART2_BASE, hal::SEND>() {
	return DMA1_Channel7_BASE;
}

template <>
constexpr uintptr_t hal::getDMAChannelForSerial<USART2_BASE, hal::RECEIVE>() {
	return DMA1_Channel6_BASE;
}

template <>
constexpr uintptr_t
hal::getDMAChannelNumberForSerial<USART2_BASE, hal::SEND>() {
	return 7;
}

template <>
constexpr uintptr_t
hal::getDMAChannelNumberForSerial<USART2_BASE, hal::RECEIVE>() {
	return 6;
}

template <> inline void hal::enableIRQs<USART2_BASE>() {
	NVIC_EnableIRQ(USART2_IRQn);
}

#define SERIAL2 USART2_EXTI26_IRQHandler
extern void (*SERIAL2_Handler)();

template <> inline void hal::setIRQ<USART2_BASE>(void (*irq)()) {
	SERIAL2_Handler = irq;
}

// Serial 3

template <> constexpr uintptr_t hal::getDMAForSerial<USART3_BASE, hal::SEND>() {
	return DMA1_BASE;
}

template <>
constexpr uintptr_t hal::getDMAForSerial<USART3_BASE, hal::RECEIVE>() {
	return DMA1_BASE;
}

template <>
constexpr uintptr_t hal::getDMAChannelForSerial<USART3_BASE, hal::SEND>() {
	return DMA1_Channel3_BASE;
}

template <>
constexpr uintptr_t hal::getDMAChannelForSerial<USART3_BASE, hal::RECEIVE>() {
	return DMA1_Channel2_BASE;
}

template <>
constexpr uintptr_t
hal::getDMAChannelNumberForSerial<USART3_BASE, hal::SEND>() {
	return 3;
}

template <>
constexpr uintptr_t
hal::getDMAChannelNumberForSerial<USART3_BASE, hal::RECEIVE>() {
	return 2;
}

template <> inline void hal::enableIRQs<USART3_BASE>() {
	NVIC_EnableIRQ(USART3_IRQn);
}

#define SERIAL3 USART3_EXTI28_IRQHandler
extern void (*SERIAL3_Handler)();

template <> inline void hal::setIRQ<USART3_BASE>(void (*irq)()) {
	SERIAL3_Handler = irq;
}

// Serial 4

template <> constexpr uintptr_t hal::getDMAForSerial<UART4_BASE, hal::SEND>() {
	return DMA2_BASE;
}

template <>
constexpr uintptr_t hal::getDMAForSerial<UART4_BASE, hal::RECEIVE>() {
	return DMA2_BASE;
}

template <>
constexpr uintptr_t hal::getDMAChannelForSerial<UART4_BASE, hal::SEND>() {
	return DMA2_Channel5_BASE;
}

template <>
constexpr uintptr_t hal::getDMAChannelForSerial<UART4_BASE, hal::RECEIVE>() {
	return DMA2_Channel3_BASE;
}

template <>
constexpr uintptr_t hal::getDMAChannelNumberForSerial<UART4_BASE, hal::SEND>() {
	return 5;
}

template <>
constexpr uintptr_t
hal::getDMAChannelNumberForSerial<UART4_BASE, hal::RECEIVE>() {
	return 3;
}

#define SERIAL4 UART4_EXTI34_IRQHandler
extern void (*SERIAL4_Handler)();

template <> inline void hal::setIRQ<UART4_BASE>(void (*irq)()) {
	SERIAL4_Handler = irq;
}

// Serial 5
#define SERIAL5 UART5_EXTI35_IRQHandler
extern void (*SERIAL5_Handler)();

template <> inline void hal::setIRQ<UART5_BASE>(void (*irq)()) {
	SERIAL5_Handler = irq;
}

template <> inline void hal::enableIRQs<UART4_BASE>() {
	NVIC_EnableIRQ(UART4_IRQn);
}

template <> inline void hal::enableIRQs<UART5_BASE>() {
	NVIC_EnableIRQ(UART5_IRQn);
}

// General

template <uintptr_t uart, uintptr_t dma_channel, hal::Direction dir>
void hal::confUART_DMA_CPAR() {
	DMA_Channel_TypeDef *dma = hal::castDMACh<dma_channel>();
	USART_TypeDef *usart = reinterpret_cast<USART_TypeDef *>(uart);
	if constexpr (dir == hal::SEND)
		dma->CPAR = (uintptr_t)&usart->TDR;
	else if constexpr (dir == hal::RECEIVE)
		dma->CPAR = (uintptr_t)&usart->RDR;
}

template <uintptr_t base, size_t sendSize, size_t recvSize, bool dma,
		  u32 rcc_reg>
void hal::confSerial(u32 baud, hal::UART_Parity) {
	USART_TypeDef *usart = reinterpret_cast<USART_TypeDef *>(base);
	usart->CR1 = 0;
	u32 clock = getClock<rcc_reg>();
	u16 brr = clock / baud;
	usart->BRR = brr;

	u32 cr1 = 0;
	if constexpr (sendSize > 0)
		cr1 |= USART_CR1_TE;
	if constexpr (recvSize > 0) {
		cr1 |= USART_CR1_RE;
		if constexpr (!dma) {
			usart->RQR = USART_RQR_RXFRQ;
			cr1 |= USART_CR1_RXNEIE;
		}
	}

	usart->CR1 = cr1;
	usart->CR2 = 0;
	if constexpr (dma)
		usart->CR3 = USART_CR3_DMAR | USART_CR3_DMAT;
	else
		usart->CR3 = 0;
	usart->CR1 |= USART_CR1_UE;
}

template <> inline void hal::enableDMAClock<DMA1_BASE>() {
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
}

template <uintptr_t uart> inline uint8_t hal::UART_Acc<uart>::get() {
	return reinterpret_cast<USART_TypeDef *>(uart)->RDR;
}

template <uintptr_t uart> inline void hal::UART_Acc<uart>::put(uint8_t c) {
	reinterpret_cast<USART_TypeDef *>(uart)->TDR = c;
}

template <uintptr_t uart> inline void handleORE() {
	USART_TypeDef *usart = reinterpret_cast<USART_TypeDef*>(uart);
	usart->ICR = USART_ICR_ORECF;

}
