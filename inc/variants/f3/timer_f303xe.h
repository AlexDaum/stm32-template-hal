#pragma once

#include "timer.h"

namespace hal {

using _TIM1 = Timer<TIM1_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB2ENR),
					RCC_APB2ENR_TIM1EN, 6, timer_type::ADV_CTR, uint16_t>;

using _TIM8 = Timer<TIM8_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB2ENR),
					RCC_APB2ENR_TIM8EN, 6, timer_type::ADV_CTR, uint16_t>;

using _TIM2 = Timer<TIM2_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB1ENR),
					RCC_APB1ENR_TIM2EN, 4, timer_type::GEN_PURP, uint32_t>;

using _TIM3 = Timer<TIM3_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB1ENR),
					RCC_APB1ENR_TIM3EN, 4, timer_type::GEN_PURP, uint16_t>;

using _TIM4 = Timer<TIM4_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB1ENR),
					RCC_APB1ENR_TIM4EN, 4, timer_type::GEN_PURP, uint16_t>;

using _TIM6 = Timer<TIM6_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB1ENR),
					RCC_APB1ENR_TIM6EN, 0, timer_type::BASIC, uint16_t>;

using _TIM7 = Timer<TIM7_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB1ENR),
					RCC_APB1ENR_TIM7EN, 0, timer_type::BASIC, uint16_t>;

using _TIM15 = Timer<TIM15_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB2ENR),
					 RCC_APB2ENR_TIM15EN, 2, timer_type::GEN_PURP, uint16_t>;

using _TIM16 = Timer<TIM16_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB2ENR),
					 RCC_APB2ENR_TIM16EN, 2, timer_type::GEN_PURP, uint16_t>;

using _TIM17 = Timer<TIM17_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB2ENR),
					 RCC_APB2ENR_TIM17EN, 2, timer_type::GEN_PURP, uint16_t>;

} // namespace hal
