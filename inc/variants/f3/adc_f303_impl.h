#pragma once
#include "ADC.h"
#include "stm.h"

template <uintptr_t adc>
constexpr DMA_Channel_TypeDef *getDMAChannel() {
  return nullptr;
}

template <uintptr_t adc>
void enableDMA() {
  if constexpr (adc == ADC1_BASE)
    RCC->AHBENR |= RCC_AHBENR_DMA1EN;
  else
    RCC->AHBENR |= RCC_AHBENR_DMA2EN;
}

template <>
constexpr DMA_Channel_TypeDef *getDMAChannel<ADC1_BASE>() {
  return reinterpret_cast<DMA_Channel_TypeDef *>(DMA1_Channel1_BASE);
}

template <>
constexpr DMA_Channel_TypeDef *getDMAChannel<ADC2_BASE>() {
  return reinterpret_cast<DMA_Channel_TypeDef *>(DMA2_Channel1_BASE);
}

template <>
constexpr DMA_Channel_TypeDef *getDMAChannel<ADC3_BASE>() {
  return reinterpret_cast<DMA_Channel_TypeDef *>(DMA2_Channel5_BASE);
}

template <>
constexpr DMA_Channel_TypeDef *getDMAChannel<ADC4_BASE>() {
  return reinterpret_cast<DMA_Channel_TypeDef *>(DMA2_Channel2_BASE);
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::confSingle(int channel,
                                                     ADC_Sampletimes sample) {
  this->dma = false;

  reg()->SQR1 =
      channel << ADC_SQR1_SQ1_Pos;  // select channel and set Length to 1

  if (channel < 10)
    reg()->SMPR1 = sample << (3 * channel);
  else
    reg()->SMPR2 = sample << (3 * (channel - 10));
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::startConv() {
  if (reg()->CR & ADC_CR_ADEN) {
    reg()->ISR = ADC_ISR_EOS;  // Clear EOS
    reg()->CR |= ADC_CR_ADSTART;
  }
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
uint16_t hal::ADConv<adc, rcc_reg, rcc_mask>::readSingle() {
  if (reg()->CR & ADC_CR_ADEN) {
    wait();
    return reg()->DR;
  }
  return 0;
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::wait() {
  while (!(reg()->ISR & ADC_ISR_EOS))
    ;
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::stop() {
  if (reg()->CR & ADC_CR_ADEN) {
    reg()->CR |= ADC_CR_ADDIS;
    while (reg()->CR & ADC_CR_ADDIS)
      ;
  }
}

template <uintptr_t adc_reg, uintptr_t rcc_reg, u32 rcc_mask>
void confDMA(hal::ADConv<adc_reg, rcc_reg, rcc_mask> &adc, u32 num,
             int buffSize, bool cont, uint16_t *target,
             hal::ADC_Sampletimes sample) {
  enableDMA<adc_reg>();
  auto dmach = getDMAChannel<adc_reg>();
  dmach->CPAR = (uint32_t)((&adc.reg()->DR));
  dmach->CMAR = (uint32_t)((target));
  dmach->CNDTR = buffSize;
  uint32_t ccr = DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE | DMA_CCR_MINC | DMA_CCR_EN;
  ccr |= DMA_CCR_CIRC;
  DMA1_Channel1->CCR = ccr;

  u32 sqr[4] = {(u32)(num - 1), 0, 0, 0};
  u32 smpr[2] = {0, 0};
  for (size_t i = 1; i <= num; i++) {
    u16 tar = target[i - 1];
    sqr[i / 5] |= tar << (ADC_SQR1_SQ1_Pos * (i % 5));
    smpr[tar / 10] |= sample << (ADC_SMPR1_SMP1_Pos * (tar % 10));
  }
  adc.reg()->SQR1 = sqr[0];
  adc.reg()->SQR2 = sqr[1];
  adc.reg()->SQR3 = sqr[2];
  adc.reg()->SQR4 = sqr[3];
  adc.reg()->SMPR1 = smpr[0];
  adc.reg()->SMPR2 = smpr[1];

  uint32_t cfgr = adc.reg()->CFGR;
  cfgr |= ADC_CFGR_DMAEN;
  if (cont) cfgr |= ADC_CFGR_CONT | ADC_CFGR_DMACFG;

  adc.reg()->CFGR = cfgr;
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::extraEnable() {
  reg()->CR &= ~ADC_CR_ADVREGEN;
  reg()->CR |= ADC_CR_ADVREGEN_0;
  if constexpr (adc == ADC1_BASE)
    ADC12_COMMON->CCR = ADC_CCR_CKMODE_0;
  else
    ADC34_COMMON->CCR = ADC_CCR_CKMODE_0;
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::confDMASweep(uint16_t *target,
                                                       int num, int mult,
                                                       ADC_Sampletimes *sample,
                                                       bool cont) {
  stop();
  uint16_t mask = 0;
  for (int i = 0; i < num; i++) {
    mask |= 1 << target[i];
  }
  this->dma = true;
  confDMA(*this, num, num * mult, cont, target, *sample);
  start();
  startConv();
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::confDMASweep(uint16_t *target,
                                                       int num, int mult,
                                                       ADC_Sampletimes sample,
                                                       bool cont) {
  stop();
  this->dma = true;
  confDMA(*this, num, num * mult, cont, target, sample);
  start();
  startConv();
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
template <size_t num>
void hal::ADConv<adc, rcc_reg, rcc_mask>::confDMASweep(
    std::array<uint16_t, num> &target, int mult,
    const std::array<ADC_Sampletimes, num> &sample, bool cont) {
  stop();
  uint16_t mask = 0;
  for (int i = 0; i < num; i++) {
    mask |= 1 << target[i];
  }
  this->dma = true;
  confDMA(*this, num, cont, mask, target, sample[0]);
  start();
  startConv();
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
template <size_t num>
void hal::ADConv<adc, rcc_reg, rcc_mask>::confDMASweep(
    std::array<uint16_t, num> &target, int mult, ADC_Sampletimes sample,
    bool cont) {
  stop();
  this->dma = true;
  confDMA<adc, rcc_reg, rcc_mask>(*this, num / mult, num, cont, target.begin(),
                                  sample);
  start();
  startConv();
}
