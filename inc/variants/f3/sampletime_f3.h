#include <cstdint>
namespace hal {

enum ADC_Sampletimes : uint16_t {
	Smpl1_5,
	Smpl2_5,
	Smpl4_5,
	Smpl7_5,
	Smpl19_5,
	Smpl61_5,
	Smpl181_5,
	Smpl601_5
};

}
