#pragma once
#include "ADC.h"
namespace hal {
using _ADC1 = ADConv<ADC1_BASE, RCC_BASE + offsetof(RCC_TypeDef, AHBENR),
					 RCC_AHBENR_ADC12EN>;
using _ADC2 = ADConv<ADC2_BASE, RCC_BASE + offsetof(RCC_TypeDef, AHBENR),
					 RCC_AHBENR_ADC12EN>;
using _ADC3 = ADConv<ADC3_BASE, RCC_BASE + offsetof(RCC_TypeDef, AHBENR),
					 RCC_AHBENR_ADC34EN>;
using _ADC4 = ADConv<ADC4_BASE, RCC_BASE + offsetof(RCC_TypeDef, AHBENR),
					 RCC_AHBENR_ADC34EN>;
} // namespace hal
