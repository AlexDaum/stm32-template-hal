
#pragma once

#include "serial.h"

namespace hal {

template <size_t recvSize, int sendSize, bool dma>
using _Serial1 = Serial<USART1_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB2ENR),
						RCC_APB2ENR_USART1EN, sendSize, recvSize, dma>;

template <size_t recvSize, int sendSize, bool dma>
using _Serial2 = Serial<USART2_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB1ENR),
						RCC_APB1ENR_USART2EN, sendSize, recvSize, dma>;

template <size_t recvSize, int sendSize, bool dma>
using _Serial3 = Serial<USART3_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB1ENR),
						RCC_APB1ENR_USART3EN, sendSize, recvSize, dma>;

template <size_t recvSize, int sendSize, bool dma>
using _Serial4 = Serial<UART4_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB1ENR),
						RCC_APB1ENR_UART4EN, sendSize, recvSize, dma>;

template <size_t recvSize, int sendSize>
using _Serial5 = Serial<USART1_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB1ENR),
						RCC_APB1ENR_UART5EN, sendSize, recvSize, false>;

} // namespace hal
