#pragma once
#include "../timer.h"

#if defined STM32F030x6 | defined STM32F030F4Px
#include "f0/timer_f030x6.h"
#elif defined STM32F303RETx
#include "f3/timer_f303xe.h"
#else
#error "No known STM32 Device"
#endif
