#pragma once

#if defined STM32F030x6 | defined STM32F030F4Px
#include "f0/adc_f030_impl.h"
#include "f0/adc_f030x6.h"
#elif defined STM32F303RETx
#include "f3/adc_f303_impl.h"
#include "f3/adc_f303.h"
#else
#error "No known STM32 Device"
#endif
