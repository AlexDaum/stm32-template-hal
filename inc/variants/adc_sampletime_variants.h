#pragma once

#ifdef STM32F0
#include "f0/sampletime_f0.h"
#elif defined STM32F3
#include "f3/sampletime_f3.h"
#endif
