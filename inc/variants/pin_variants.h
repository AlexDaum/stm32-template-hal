/*
 * pin_variants.h
 *
 *  Created on: 15.11.2018
 *      Author: alex
 */

#pragma once

#if defined STM32F030F4Px
#define HAS_GPIOA
#define HAS_GPIOB
#define HAS_GPIOF
#elif defined STM32F303RETx
#define HAS_GPIOA
#define HAS_GPIOB
#define HAS_GPIOC
#define HAS_GPIOD
#else
#error "No Known STM32 Device"
#endif
