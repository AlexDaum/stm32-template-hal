/*
 * timer_f030f4.h
 *
 *  Created on: 22.12.2018
 *      Author: alex
 */

#ifndef INC_VARIANTS_TIMER_F030F4_H_
#define INC_VARIANTS_TIMER_F030x6_H_

#include "stm32f030x6.h"
#include "timer.h"

namespace hal {
typedef Timer<TIM1_BASE, APB2CLOCK, RCC_APB2ENR_TIM1EN, 4, ADV_CTR, uint16_t>
	_TIM1;

typedef Timer<TIM3_BASE, APB1CLOCK, RCC_APB1ENR_TIM3EN, 4, GEN_PURP, uint16_t>
	_TIM3;

typedef Timer<TIM14_BASE, APB1CLOCK, RCC_APB1ENR_TIM14EN, 1, GEN_PURP, uint16_t>
	_TIM14;

typedef Timer<TIM16_BASE, APB1CLOCK, RCC_APB1ENR_TIM14EN, 1, GEN_PURP, uint16_t>
	_TIM16;
} // namespace hal
//_TIM16 Timer16;

#endif /* INC_VARIANTS_TIMER_F030F4_H_ */
