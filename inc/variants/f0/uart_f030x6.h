#pragma once
#include "serial.h"
#include "stm.h"

namespace hal {

template <size_t sendSize, int recvSize, bool dma>
using _Serial1 =
	hal::Serial<USART1_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB2ENR),
				RCC_APB2ENR_USART1EN, sendSize, recvSize, dma>;
}
