#include <cstdint>
namespace hal {

enum ADC_Sampletimes : uint16_t {
	Smpl1_5,
	Smpl7_5,
	Smpl13_5,
	Smpl28_5,
	Smpl41_5,
	Smpl55_5,
	Smpl71_5,
	Smpl239_5
};

}
