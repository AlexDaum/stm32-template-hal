/*
 * adc_f030x6.h
 *
 *  Created on: 23.12.2018
 *      Author: alex
 */

#ifndef INC_VARIANTS_ADC_F030X6_H_
#define INC_VARIANTS_ADC_F030X6_H_
#include "ADC.h"

namespace hal {
typedef ADConv<ADC1_BASE, RCC_BASE + offsetof(RCC_TypeDef, APB2ENR),
			   RCC_APB2ENR_ADC1EN>
	_ADC1;
}

#endif /* INC_VARIANTS_ADC_F030X6_H_ */
