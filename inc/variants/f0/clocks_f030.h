/*
 * clocks.h
 *
 *  Created on: 17.11.2018
 *      Author: alex
 */

#pragma once

#include "stm.h"
#include <stddef.h>

namespace hal {

#ifndef RCC_CFGR_PPRE_Pos
constexpr uint32_t RCC_CFGR_PPRE_Pos = 32 - __CLZ(RCC_CFGR_PPRE_0);
#endif

constexpr uintptr_t APB1CLOCK =
	(uintptr_t)(RCC_BASE + offsetof(RCC_TypeDef, APB1ENR));

constexpr uintptr_t APB2CLOCK =
	(uintptr_t)(RCC_BASE + offsetof(RCC_TypeDef, APB2ENR));

constexpr int apb_prescaler[] = {[0b000] = 1, [0b001] = 1, [0b010] = 1,
								 [0b011] = 1, [0b100] = 2, [0b101] = 4,
								 [0b110] = 8, [0b111] = 16};
template <uintptr_t clockdomain> uint32_t getClock() { return 0; }

template <> inline uint32_t getClock<APB1CLOCK>() {
	uint32_t apb2_presc = RCC->CFGR & RCC_CFGR_PPRE;
	apb2_presc >>= RCC_CFGR_PPRE_Pos;
	return SystemCoreClock / apb_prescaler[apb2_presc];
}

template <> inline uint32_t getClock<APB2CLOCK>() {
	uint32_t apb2_presc = RCC->CFGR & RCC_CFGR_PPRE;
	apb2_presc >>= RCC_CFGR_PPRE_Pos;
	return SystemCoreClock / apb_prescaler[apb2_presc];
}

}
