#pragma once
#include "ADC.h"
#include <array>

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::extraEnable() {}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::confSingle(int channel,
													 ADC_Sampletimes sample) {
	this->dma = false;
	reg()->CHSELR = 1 << channel;
	reg()->SMPR = sample;
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::startConv() {
	if (reg()->CR & ADC_CR_ADEN) {
		while (!(reg()->ISR & ADC_ISR_ADRDY))
			;
		reg()->CR |= ADC_CR_ADSTART;
	}
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
uint16_t hal::ADConv<adc, rcc_reg, rcc_mask>::readSingle() {
	wait();
	return reg()->DR;
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::wait() {
	while (reg()->CR & ADC_CR_ADSTART)
		;
}
template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::stop() {
	if (reg()->CR & ADC_CR_ADEN) {
		reg()->CR |= ADC_CR_ADDIS;
		while (reg()->CR & ADC_CR_ADDIS)
			;
	}
}

template <uintptr_t adc_reg, uintptr_t rcc_reg, uint32_t rcc_mask>
void confDMA(hal::ADConv<adc_reg, rcc_reg, rcc_mask> &adc, size_t num, bool cont,
			 uint16_t mask, uint16_t *target, hal::ADC_Sampletimes sample) {

	RCC->AHBENR |= RCC_AHBENR_DMA1EN;

	DMA1_Channel1->CPAR = (uint32_t)((&adc.reg()->DR));
	DMA1_Channel1->CMAR = (uint32_t)((target));
	DMA1_Channel1->CNDTR = num;
	uint32_t ccr = DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE | DMA_CCR_MINC | DMA_CCR_EN;
	ccr |= DMA_CCR_CIRC;
	DMA1_Channel1->CCR = ccr;
	adc.reg()->CHSELR = mask;
	adc.reg()->SMPR = sample;
	uint32_t cfgr1 = adc.reg()->CFGR1;
	cfgr1 |= ADC_CFGR1_DMAEN;
	if (cont)
		cfgr1 |= ADC_CFGR1_CONT | ADC_CFGR1_DMACFG;

	adc.reg()->CFGR1 = cfgr1;
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::confDMASweep(uint16_t *target,
													   int num, int mult,
													   ADC_Sampletimes *sample,
													   bool cont) {
	stop();
	uint16_t mask = 0;
	for (int i = 0; i < num; i++) {
		mask |= 1 << target[i];
	}
	this->dma = true;
	confDMA(*this, num, cont, mask, target, *sample);
	start();
	startConv();
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
void hal::ADConv<adc, rcc_reg, rcc_mask>::confDMASweep(uint16_t *target,
													   int num, int mult,
													   ADC_Sampletimes sample,
													   bool cont) {
	stop();
	uint16_t mask = 0;
	for (int i = 0; i < num; i++) {
		mask |= 1 << target[i];
	}
	this->dma = true;
	confDMA(*this, num, cont, mask, target, sample);
	start();
	startConv();
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
template <size_t num>
void hal::ADConv<adc, rcc_reg, rcc_mask>::confDMASweep(
	std::array<uint16_t, num> &target, int mult,
	const std::array<ADC_Sampletimes, num> &sample, bool cont) {

	stop();
	uint16_t mask = 0;
	for (int i = 0; i < num; i++) {
		mask |= 1 << target[i];
	}
	this->dma = true;
	confDMA(*this, num, cont, mask, target, sample[0]);
	start();
	startConv();
}

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask>
template <size_t num>
void hal::ADConv<adc, rcc_reg, rcc_mask>::confDMASweep(
	std::array<uint16_t, num> &target, int mult, ADC_Sampletimes sample,
	bool cont) {

	stop();
	uint16_t mask = 0;
	for (size_t i = 0; i < num; i++) {
		mask |= 1 << target[i];
	}
	this->dma = true;
	confDMA(*this, num, cont, mask, target.begin(), sample);
	start();
	startConv();
}
