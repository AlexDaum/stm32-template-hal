/*
 * Pins.h
 *
 *  Created on: 11.05.2018
 *      Author: alex
 */

#ifndef PINS_H_
#define PINS_H_

#include "stm.h"
#include <stddef.h>
#include <stdint.h>

#include "variants/pin_variants.h"

namespace hal {

template <uintptr_t g, int num> class Pin;

template <uintptr_t gpio> constexpr int getGPIONum();

template <uintptr_t g, uintptr_t rcc_reg, uint32_t rcc_mask> class GPIO {
  private:
	static constexpr GPIO_TypeDef *gpio() {
		return reinterpret_cast<GPIO_TypeDef *>(g);
	}

	static constexpr uintptr_t afr_pos() {
		return g + offsetof(GPIO_TypeDef, AFR);
	}

	static constexpr uintptr_t moder_pos() {
		return g + offsetof(GPIO_TypeDef, MODER);
	}

  public:
	void enable() const { *((volatile uint32_t *)(rcc_reg)) |= rcc_mask; }
	void disable() const { *((volatile uint32_t *)(rcc_reg)) &= ~rcc_mask; }
	void operator=(uint16_t data) const { gpio()->ODR = data; }

	void operator|=(uint16_t data) const { gpio()->BSRR = data; }

	void operator&=(uint16_t data) const { gpio()->BRR = ~data; }

	void operator^=(uint16_t data) const { gpio()->ODR ^= data; }

	void operator+=(uint16_t data) const { gpio()->BSRR = data; }

	void operator-=(uint16_t data) const { gpio()->BRR = data; }

	operator uint16_t() const { return gpio()->IDR; }
};

template <uintptr_t g, int num> class Pin {
  private:
	static constexpr GPIO_TypeDef *gpio() {
		return reinterpret_cast<GPIO_TypeDef *>(g);
	}

  public:
	constexpr const GPIO_TypeDef *getGPIO() { return gpio(); }
	constexpr int getNum() { return num; }

	void set() const { gpio()->BSRR = 1 << num; }

	void reset() const { gpio()->BRR = 1 << num; }

	void toggle() const { gpio()->ODR ^= 1 << num; }

	void output() const {
		uint32_t mo = gpio()->MODER;
		mo &= ~(0b11 << 2 * num);
		mo |= 0b01 << 2 * num;
		gpio()->MODER = mo;
	}

	void input() const { gpio()->MODER &= ~(0b11 << 2 * num); }

	void analog() const { gpio()->MODER |= 0b11 << 2 * num; }

	void alternate() const {
		gpio()->MODER &= ~(0b11 << 2 * num);
		gpio()->MODER |= 0b10 << 2 * num;
	}

	void alt(int mode) const {
		alternate();
		gpio()->AFR[num / 8] &= ~(0xF << 4 * (num % 8));
		gpio()->AFR[num / 8] |= mode << 4 * (num % 8);
	}

	void operator=(int o) const {
		if (o)
			set();
		else
			reset();
	}

	constexpr operator uint16_t() const { return 1 << num; }
};
#ifdef HAS_GPIOA
typedef const hal::Pin<GPIOA_BASE, 0> _PA0;
_PA0 PA0;
typedef const hal::Pin<GPIOA_BASE, 1> _PA1;
_PA1 PA1;
typedef const hal::Pin<GPIOA_BASE, 2> _PA2;
_PA2 PA2;
typedef const hal::Pin<GPIOA_BASE, 3> _PA3;
_PA3 PA3;
typedef const hal::Pin<GPIOA_BASE, 4> _PA4;
_PA4 PA4;
typedef const hal::Pin<GPIOA_BASE, 5> _PA5;
_PA5 PA5;
typedef const hal::Pin<GPIOA_BASE, 6> _PA6;
_PA6 PA6;
typedef const hal::Pin<GPIOA_BASE, 7> _PA7;
_PA7 PA7;
typedef const hal::Pin<GPIOA_BASE, 8> _PA8;
_PA8 PA8;
typedef const hal::Pin<GPIOA_BASE, 9> _PA9;
_PA9 PA9;
typedef const hal::Pin<GPIOA_BASE, 10> _PA10;
_PA10 PA10;
typedef const hal::Pin<GPIOA_BASE, 11> _PA11;
_PA11 PA11;
typedef const hal::Pin<GPIOA_BASE, 12> _PA12;
_PA12 PA12;
typedef const hal::Pin<GPIOA_BASE, 13> _PA13;
_PA13 PA13;
typedef const hal::Pin<GPIOA_BASE, 14> _PA14;
_PA14 PA14;
typedef const hal::Pin<GPIOA_BASE, 15> _PA15;
_PA15 PA15;
#endif
#ifdef HAS_GPIOB
typedef const hal::Pin<GPIOB_BASE, 0> _PB0;
_PB0 PB0;
typedef const hal::Pin<GPIOB_BASE, 1> _PB1;
_PB1 PB1;
typedef const hal::Pin<GPIOB_BASE, 2> _PB2;
_PB2 PB2;
typedef const hal::Pin<GPIOB_BASE, 3> _PB3;
_PB3 PB3;
typedef const hal::Pin<GPIOB_BASE, 4> _PB4;
_PB4 PB4;
typedef const hal::Pin<GPIOB_BASE, 5> _PB5;
_PB5 PB5;
typedef const hal::Pin<GPIOB_BASE, 6> _PB6;
_PB6 PB6;
typedef const hal::Pin<GPIOB_BASE, 7> _PB7;
_PB7 PB7;
typedef const hal::Pin<GPIOB_BASE, 8> _PB8;
_PB8 PB8;
typedef const hal::Pin<GPIOB_BASE, 9> _PB9;
_PB9 PB9;
typedef const hal::Pin<GPIOB_BASE, 10> _PB10;
_PB10 PB10;
typedef const hal::Pin<GPIOB_BASE, 11> _PB11;
_PB11 PB11;
typedef const hal::Pin<GPIOB_BASE, 12> _PB12;
_PB12 PB12;
typedef const hal::Pin<GPIOB_BASE, 13> _PB13;
_PB13 PB13;
typedef const hal::Pin<GPIOB_BASE, 14> _PB14;
_PB14 PB14;
typedef const hal::Pin<GPIOB_BASE, 15> _PB15;
_PB15 PB15;
#endif
#ifdef HAS_GPIOC
typedef const hal::Pin<GPIOC_BASE, 0> _PC0;
_PC0 PC0;
typedef const hal::Pin<GPIOC_BASE, 1> _PC1;
_PC1 PC1;
typedef const hal::Pin<GPIOC_BASE, 2> _PC2;
_PC2 PC2;
typedef const hal::Pin<GPIOC_BASE, 3> _PC3;
_PC3 PC3;
typedef const hal::Pin<GPIOC_BASE, 4> _PC4;
_PC4 PC4;
typedef const hal::Pin<GPIOC_BASE, 5> _PC5;
_PC5 PC5;
typedef const hal::Pin<GPIOC_BASE, 6> _PC6;
_PC6 PC6;
typedef const hal::Pin<GPIOC_BASE, 7> _PC7;
_PC7 PC7;
typedef const hal::Pin<GPIOC_BASE, 8> _PC8;
_PC8 PC8;
typedef const hal::Pin<GPIOC_BASE, 9> _PC9;
_PC9 PC9;
typedef const hal::Pin<GPIOC_BASE, 10> _PC10;
_PC10 PC10;
typedef const hal::Pin<GPIOC_BASE, 11> _PC11;
_PC11 PC11;
typedef const hal::Pin<GPIOC_BASE, 12> _PC12;
_PC12 PC12;
typedef const hal::Pin<GPIOC_BASE, 13> _PC13;
_PC13 PC13;
typedef const hal::Pin<GPIOC_BASE, 14> _PC14;
_PC14 PC14;
typedef const hal::Pin<GPIOC_BASE, 15> _PC15;
_PC15 PC15;
#endif
#ifdef HAS_GPIOD
typedef const hal::Pin<GPIOD_BASE, 0> _PD0;
_PD0 PD0;
typedef const hal::Pin<GPIOD_BASE, 1> _PD1;
_PD1 PD1;
typedef const hal::Pin<GPIOD_BASE, 2> _PD2;
_PD2 PD2;
typedef const hal::Pin<GPIOD_BASE, 3> _PD3;
_PD3 PD3;
typedef const hal::Pin<GPIOD_BASE, 4> _PD4;
_PD4 PD4;
typedef const hal::Pin<GPIOD_BASE, 5> _PD5;
_PD5 PD5;
typedef const hal::Pin<GPIOD_BASE, 6> _PD6;
_PD6 PD6;
typedef const hal::Pin<GPIOD_BASE, 7> _PD7;
_PD7 PD7;
typedef const hal::Pin<GPIOD_BASE, 8> _PD8;
_PD8 PD8;
typedef const hal::Pin<GPIOD_BASE, 9> _PD9;
_PD9 PD9;
typedef const hal::Pin<GPIOD_BASE, 10> _PD10;
_PD10 PD10;
typedef const hal::Pin<GPIOD_BASE, 11> _PD11;
_PD11 PD11;
typedef const hal::Pin<GPIOD_BASE, 12> _PD12;
_PD12 PD12;
typedef const hal::Pin<GPIOD_BASE, 13> _PD13;
_PD13 PD13;
typedef const hal::Pin<GPIOD_BASE, 14> _PD14;
_PD14 PD14;
typedef const hal::Pin<GPIOD_BASE, 15> _PD15;
_PD15 PD15;
#endif
#ifdef HAS_GPIOE
typedef const hal::Pin<GPIOE_BASE, 0> _PE0;
_PE0 PE0;
typedef const hal::Pin<GPIOE_BASE, 1> _PE1;
_PE1 PE1;
typedef const hal::Pin<GPIOE_BASE, 2> _PE2;
_PE2 PE2;
typedef const hal::Pin<GPIOE_BASE, 3> _PE3;
_PE3 PE3;
typedef const hal::Pin<GPIOE_BASE, 4> _PE4;
_PE4 PE4;
typedef const hal::Pin<GPIOE_BASE, 5> _PE5;
_PE5 PE5;
typedef const hal::Pin<GPIOE_BASE, 6> _PE6;
_PE6 PE6;
typedef const hal::Pin<GPIOE_BASE, 7> _PE7;
_PE7 PE7;
typedef const hal::Pin<GPIOE_BASE, 8> _PE8;
_PE8 PE8;
typedef const hal::Pin<GPIOE_BASE, 9> _PE9;
_PE9 PE9;
typedef const hal::Pin<GPIOE_BASE, 10> _PE10;
_PE10 PE10;
typedef const hal::Pin<GPIOE_BASE, 11> _PE11;
_PE11 PE11;
typedef const hal::Pin<GPIOE_BASE, 12> _PE12;
_PE12 PE12;
typedef const hal::Pin<GPIOE_BASE, 13> _PE13;
_PE13 PE13;
typedef const hal::Pin<GPIOE_BASE, 14> _PE14;
_PE14 PE14;
typedef const hal::Pin<GPIOE_BASE, 15> _PE15;
_PE15 PE15;
#endif
#ifdef HAS_GPIOF
typedef const hal::Pin<GPIOF_BASE, 0> _PF0;
_PF0 PF0;
typedef const hal::Pin<GPIOF_BASE, 1> _PF1;
_PF1 PF1;
#ifndef HAS_GPIOF_SMALL
typedef const hal::Pin<GPIOF_BASE, 2> _PF2;
_PF2 PF2;
typedef const hal::Pin<GPIOF_BASE, 3> _PF3;
_PF3 PF3;
typedef const hal::Pin<GPIOF_BASE, 4> _PF4;
_PF4 PF4;
typedef const hal::Pin<GPIOF_BASE, 5> _PF5;
_PF5 PF5;
typedef const hal::Pin<GPIOF_BASE, 6> _PF6;
_PF6 PF6;
typedef const hal::Pin<GPIOF_BASE, 7> _PF7;
_PF7 PF7;
typedef const hal::Pin<GPIOF_BASE, 8> _PF8;
_PF8 PF8;
typedef const hal::Pin<GPIOF_BASE, 9> _PF9;
_PF9 PF9;
typedef const hal::Pin<GPIOF_BASE, 10> _PF10;
_PF10 PF10;
typedef const hal::Pin<GPIOF_BASE, 11> _PF11;
_PF11 PF11;
typedef const hal::Pin<GPIOF_BASE, 12> _PF12;
_PF12 PF12;
typedef const hal::Pin<GPIOF_BASE, 13> _PF13;
_PF13 PF13;
typedef const hal::Pin<GPIOF_BASE, 14> _PF14;
_PF14 PF14;
typedef const hal::Pin<GPIOF_BASE, 15> _PF15;
_PF15 PF15;
#endif
#endif
#ifdef HAS_GPIOG
typedef const hal::Pin<GPIOG_BASE, 0> _PG0;
_PG0 PG0;
typedef const hal::Pin<GPIOG_BASE, 1> _PG1;
_PG1 PG1;
typedef const hal::Pin<GPIOG_BASE, 2> _PG2;
_PG2 PG2;
typedef const hal::Pin<GPIOG_BASE, 3> _PG3;
_PG3 PG3;
typedef const hal::Pin<GPIOG_BASE, 4> _PG4;
_PG4 PG4;
typedef const hal::Pin<GPIOG_BASE, 5> _PG5;
_PG5 PG5;
typedef const hal::Pin<GPIOG_BASE, 6> _PG6;
_PG6 PG6;
typedef const hal::Pin<GPIOG_BASE, 7> _PG7;
_PG7 PG7;
typedef const hal::Pin<GPIOG_BASE, 8> _PG8;
_PG8 PG8;
typedef const hal::Pin<GPIOG_BASE, 9> _PG9;
_PG9 PG9;
typedef const hal::Pin<GPIOG_BASE, 10> _PG10;
_PG10 PG10;
typedef const hal::Pin<GPIOG_BASE, 11> _PG11;
_PG11 PG11;
typedef const hal::Pin<GPIOG_BASE, 12> _PG12;
_PG12 PG12;
typedef const hal::Pin<GPIOG_BASE, 13> _PG13;
_PG13 PG13;
typedef const hal::Pin<GPIOG_BASE, 14> _PG14;
_PG14 PG14;
typedef const hal::Pin<GPIOG_BASE, 15> _PG15;
_PG15 PG15;
#endif

#ifdef GPIOH_BASE
typedef const hal::Pin<GPIOH_BASE, 0> _PH0;
_PH0 PH0;
typedef const hal::Pin<GPIOH_BASE, 1> _PH1;
_PH1 PH1;

#endif

	const hal::Pin<0, 0>
		PIN_NONE;

#define getGPIONum_IMPL(gp, nu)                                                \
	template <> constexpr int getGPIONum<gp>() { return nu; }

#ifdef HAS_GPIOA
const hal::GPIO<GPIOA_BASE, (uintptr_t)RCC_BASE + offsetof(RCC_TypeDef, AHBENR),
				RCC_AHBENR_GPIOAEN>
	GPA;
getGPIONum_IMPL(GPIOA_BASE, 0)
#endif
#ifdef HAS_GPIOB
	const
	hal::GPIO<GPIOB_BASE, (uintptr_t)RCC_BASE + offsetof(RCC_TypeDef, AHBENR),
			  RCC_AHBENR_GPIOBEN> GPB;
getGPIONum_IMPL(GPIOB_BASE, 1)
#endif
#ifdef HAS_GPIOC
	const
	hal::GPIO<GPIOC_BASE, (uintptr_t)RCC_BASE + offsetof(RCC_TypeDef, AHBENR),
			  RCC_AHBENR_GPIOCEN> GPC;
getGPIONum_IMPL(GPIOC_BASE, 2)
#endif
#ifdef HAS_GPIOD
	const
	hal::GPIO<GPIOD_BASE, (uintptr_t)RCC_BASE + offsetof(RCC_TypeDef, AHBENR),
			  RCC_AHBENR_GPIODEN> GPD;
getGPIONum_IMPL(GPIOD_BASE, 3)
#endif
#ifdef HAS_GPIOE
	const
	hal::GPIO<GPIOE_BASE, (uintptr_t)RCC_BASE + offsetof(RCC_TypeDef, AHBENR),
			  RCC_AHBENR_GPIOEEN> GPE;
getGPIONum_IMPL(GPIOE_BASE, 4)
#endif
#ifdef HAS_GPIOF
	const
	hal::GPIO<GPIOF_BASE, (uintptr_t)RCC_BASE + offsetof(RCC_TypeDef, AHBENR),
			  RCC_AHBENR_GPIOFEN> GPF;
getGPIONum_IMPL(GPIOF_BASE, 5)
#endif
#ifdef HAS_GPIOG
	const
	hal::GPIO<GPIOG_BASE, (uintptr_t)RCC_BASE + offsetof(RCC_TypeDef, AHBENR),
			  RCC_AHBENR_GPIOGEN> GPG;
getGPIONum_IMPL(GPIOG_BASE, 6)
#endif
#ifdef HAS_GPIOH
	const
	hal::GPIO<GPIOH_BASE, (uintptr_t)RCC_BASE + offsetof(RCC_TypeDef, AHBENR),
			  RCC_AHBENR_GPIOHEN> GPH;
getGPIONum_IMPL(GPIOH_BASE, 7)
#endif

} // namespace hal
#endif /* PINS_H_ */
