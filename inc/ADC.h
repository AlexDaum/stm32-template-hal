#pragma once

#include "stm.h"
#include "variants/adc_sampletime_variants.h"
#include <array>
#include <stddef.h>
#include <stdint.h>
namespace hal {

enum ADC_Trigger {

};

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask> class ADConv;

template <uintptr_t adc, uintptr_t rcc_reg, uint32_t rcc_mask> class ADConv {
  private:
	bool dma = false;
	void extraEnable();

  public:
	static constexpr ADC_TypeDef *reg() {
		return reinterpret_cast<ADC_TypeDef *>(adc);
	}

	ADConv() { enable(); }
	~ADConv() { disable(); }

	void enable() {
		*reinterpret_cast<uint32_t *>(rcc_reg) |= rcc_mask;
		extraEnable();
	}
	void disable() { *reinterpret_cast<uint32_t *>(rcc_reg) &= ~rcc_mask; }

	void stop();
	void start() {
		reg()->CR |= ADC_CR_ADEN;
		while (!(reg()->ISR & ADC_ISR_ADRDY))
			;
	}

	bool isEnabled() { return reg()->CR & ADC_CR_ADEN; }

	void startConv();
	void wait();
	void calibrate() {
		if (reg()->CR & ADC_CR_ADEN) {
			reg()->CR |= ADC_CR_ADDIS;
			while (reg()->CR & ADC_CR_ADCAL)
				;
		}
		reg()->CR |= ADC_CR_ADCAL;
		while (reg()->CR & ADC_CR_ADCAL) {
		}
	}
	uint16_t readSingle();

	uint16_t readSingleBlocking() {
		startConv();
		wait();
		return readSingle();
	}

	void confSingle(int channel, ADC_Sampletimes sample);

	/**
	 * Configure the ADC in DMA Sweep mode, the results will be saved to the
	 * array target, where the channels to be read are stored initially
	 */
	template <size_t num>
	void confDMASweep(std::array<uint16_t, num> &target, int mult = 1,
					  ADC_Sampletimes sample = Smpl1_5, bool cont = true);
	/**
	 * Configure the ADC in DMA Sweep mode, the results will be saved to the
	 * array target, where the channels to be read are stored initially
	 */
	template <size_t num>
	void confDMASweep(std::array<uint16_t, num> &target, int mult,
					  const std::array<ADC_Sampletimes, num> &sample,
					  bool cont = true);
	/**
	 * Configure the ADC in DMA Sweep mode, the results will be saved to the
	 * array target, where the channels to be read are stored initially
	 */
	void confDMASweep(uint16_t *target, int num, int mult = 1,
					  ADC_Sampletimes sample = Smpl1_5, bool cont = true);
	/**
	 * Configure the ADC in DMA Sweep mode, the results will be saved to the
	 * array target, where the channels to be read are stored initially
	 */
	void confDMASweep(uint16_t *target, int num, int mult,
					  ADC_Sampletimes *sample, bool cont = true);
};
} // namespace hal

#include "variants/adc_variants.h"
