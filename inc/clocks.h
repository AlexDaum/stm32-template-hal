/*
 * clocks.h
 *
 *  Created on: 17.11.2018
 *      Author: alex
 */

#ifndef CLOCKS_H_
#define CLOCKS_H_

#ifdef STM32F30
#include "variants/f3/clocks_f30.h"
#elif defined STM32F0
#include "variants/f0/clocks_f030.h"
#else
#error "No Known STM32 defined"
#endif

#endif /* CLOCKS_H_ */
