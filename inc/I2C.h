#pragma once

#define I2C_BUFSIZE 32

#include "haldefs.h"
#include "ringbuffer.h"
#include "stm.h"

namespace hal {

template <uintptr_t base> void confBaud(u32 baud);
template <uintptr_t base> I2C_TypeDef *castI2C() {
	return reinterpret_cast<I2C_TypeDef *>(base);
}

template <uintptr_t base, uintptr_t rcc_reg, u32 rcc_mask> class I2C {
  private:
	/*======================================
	 * 			Type Declaration
	 *======================================*/

	using this_t = I2C<base, rcc_reg, rcc_mask>;

	/*======================================
	 * 				IRQ Handling
	 *======================================*/

	static this_t *backref;
	static void irq() {
		if (backref != nullptr)
			backref->handleIRQ();
	}

	/*======================================
	 *				Other
	 *======================================*/

	static constexpr I2C_TypeDef *i2c() {
		return reinterpret_cast<I2C_TypeDef *>(base);
	}

	/*======================================
	 *				Variables
	 *======================================*/

	RingBuffer<u8, I2C_BUFSIZE> buffer;

  public:
	I2C(u32 baud) {
		enable();
		confBauf<base>(baud);
	}

	void enable() { *reinterpret_cast<volatile u32 *>(rcc_reg) |= rcc_mask; }

	void disable() { *reinterpret_cast<volatile u32 *>(rcc_reg) &= ~rcc_mask; }

	void handleIRQ() {
		// TODO
	}
};

} // namespace hal
