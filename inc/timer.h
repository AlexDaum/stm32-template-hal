/*
 * timer.h
 *
 *  Created on: 15.11.2018
 *      Author: alex
 */

#ifndef TIMER_H_
#define TIMER_H_

#include "clocks.h"
#include "stddef.h"
#include "stm.h"
#include "timebase.h"
#include <limits>
#include <type_traits>

namespace hal {

#ifndef TIM_CCMR1_OC1M_Pos
constexpr uint32_t TIM_CCMR1_OC1M_Pos = 32 - __CLZ(TIM_CCMR1_OC1M_0);
#endif

enum timer_type { ADV_CTR, GEN_PURP, BASIC };

template <uintptr_t tmr, uintptr_t rcc_reg, uint32_t rcc_mask, int channels,
		  timer_type type, typename datatype>
class Timer {
  private:
	uint32_t getClock() const {
		u32 clock = hal::getClock<rcc_reg>();
		return clock == SystemCoreClock ? clock : clock * 2; //Timer has 2* frequency if divided
	}

	constexpr datatype max() { return std::numeric_limits<datatype>::max(); }

  public:
	Timer() {
		static_assert(std::is_integral_v<datatype>);
		enable();
	}
	~Timer() { disable(); }
	/**
	 * Enable the Timer
	 */
	void enable() const { *reinterpret_cast<uint32_t *>(rcc_reg) |= rcc_mask; }
	/**
	 * Disable the Timer
	 */
	void disable() const {
		*reinterpret_cast<uint32_t *>(rcc_reg) &= ~rcc_mask;
	}

	/**
	 * Get the Timer Register of this Timer
	 */
	static constexpr TIM_TypeDef *tim() {
		return reinterpret_cast<TIM_TypeDef *>(tmr);
	}

	template <int channel> void setCompareValue(uint32_t compare) {
		*((&tim()->CCR1) + (channel - 1)) = compare;
	}

	template <int channel> void confPWM() {
		uint32_t ccmr = *((&tim()->CCMR1) + ((channel - 1) / 2));
		ccmr &= ~(TIM_CCMR1_CC1S << (((channel - 1) % 2) * 8));
		ccmr &= ~(TIM_CCMR1_OC1M << (((channel - 1) % 2) * 8));
		ccmr |= (0b110 << TIM_CCMR1_OC1M_Pos << (((channel - 1) % 2) * 8));
		ccmr |= TIM_CCMR1_OC1FE << (((channel - 1) % 2) * 8);
		*((&tim()->CCMR1) + ((channel - 1) / 2)) = ccmr;
	}

	template <int channel>
	void setDutyCycle(uint32_t dutycycle, uint32_t fraction) {
		uint32_t comp = (tim()->ARR * dutycycle) / fraction;
		*((&tim()->CCR1) + (channel - 1)) = comp;
	}

	template <int channel>
	void setDutyCyclePow2(uint32_t dutycycle, uint32_t shift) {
		uint32_t comp = (tim()->ARR * dutycycle) >> shift;
		*((&tim()->CCR1) + (channel - 1)) = comp;
	}

	template <int channel> void setDutyCycle(float dc) {
		uint32_t comp = (uint32_t)(tim()->ARR * dc);
		*((&tim()->CCR1) + (channel - 1)) = comp;
	}

	template <int channel> void configureToggle() {
		*((&tim()->CCR1) + (channel - 1)) = 0;
		uint32_t ccmr = *((&tim()->CCMR1) + ((channel - 1) / 2));
		ccmr &= ~(TIM_CCMR1_CC1S << (((channel - 1) % 2) * 8));
		ccmr &= ~(TIM_CCMR1_OC1M << (((channel - 1) % 2) * 8));
		ccmr |= (0b011 << TIM_CCMR1_OC1M_Pos << (((channel - 1) % 2) * 8));
		*((&tim()->CCMR1) + ((channel - 1) / 2)) = ccmr;
	}

	template <int channel> void setChannelOut() {
		tim()->CCER |= TIM_CCER_CC1E << ((channel - 1) * 4);
	}

	template <int channel> void resetChannelOut() {
		tim()->CCER &= ~(TIM_CCER_CC1E << ((channel - 1) * 4));
	}

	void start() const { tim()->CR1 |= TIM_CR1_CEN; }

	void stop() const { tim()->CR1 &= ~TIM_CR1_CEN; }

	void setFrequency(int f) const {
		uint32_t freq = getClock();
		uint32_t total_frac = freq / f;
		uint32_t exp = 32 - __builtin_clz(total_frac);
		if (exp > 16) {
			total_frac >>= exp - 16;
			tim()->PSC = 1 << (exp - 16);
		}
		tim()->ARR = total_frac;
	}

	void setPeriod(int period, hal::timebase_t timebase) {
		uint32_t freq = getClock();
		uint32_t total_frac = (freq / timebase) * period;
		uint32_t exp = 32 - __builtin_clz(total_frac);
		if (exp > 16) {
			total_frac >>= exp - 16;
			tim()->PSC = 1 << (exp - 16);
		}
		tim()->ARR = total_frac;
	}
};

} // namespace hal
#include "variants/timer_variants.h"
#endif /* TIMER_H_ */
