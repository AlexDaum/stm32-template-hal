/*
 * haldefs.h
 *
 *  Created on: 28.12.2018
 *      Author: alex
 */

#ifndef INC_HALDEFS_H_
#define INC_HALDEFS_H_

#include <cstdint>
#include <cstddef>
#include <type_traits>

using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;

using s8 = int8_t;
using s16 = int16_t;
using s32 = int32_t;

template <typename t>
using isInt = std::enable_if_t<std::is_integral_v<t>>;

#endif /* INC_HALDEFS_H_ */
