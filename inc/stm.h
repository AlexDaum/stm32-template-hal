/*
 * stm.h
 *
 *  Created on: 23.12.2018
 *      Author: alex
 */

#ifndef INC_STM_H_
#define INC_STM_H_

#include "haldefs.h"

#if defined STM32F030x6 | defined STM32F030F4Px
#include <stm32f030x6.h>
#elif defined STM32F303RETx
#include "stm32f303xe.h"
#else
#error "No known STM32 Device"
#endif

#endif /* INC_STM_H_ */
