#pragma once

#include <cstddef>
#include <cstdint>

template <typename t, size_t size> class RingBuffer {
  private:
	volatile t data[size];
	volatile size_t readIdx, writeIdx;

	size_t nextIdx(size_t idx) {
		size_t next = idx + 1;
		if (next >= size)
			next = 0;
		return next;
	}

  public:
	RingBuffer() : readIdx(0), writeIdx(0) {}

	void put(t dat) {
		if constexpr (size == 0) {
			return;
		} else {
			size_t nextWrite = nextIdx(writeIdx);
			if (nextWrite != readIdx) { // if write == read => full
				data[writeIdx] = dat;
				writeIdx = nextWrite;
			}
		}
	}

	t get() {
		if constexpr (size == 0) {
			return 0;
		} else {
			t ret = 0;
			if (isNotEmpty()) {
				ret = data[readIdx];
				readIdx = nextIdx(readIdx);
			}
			return ret;
		}
	}

	bool isFull() {
		size_t nextWrite = nextIdx(writeIdx);
		return nextWrite == readIdx;
	}
	bool isNotEmpty() { return readIdx != writeIdx; }
};
