#pragma once

#include "haldefs.h"

namespace hal {

template <uintptr_t ptr> void enableDMAClock() {}

template <uintptr_t ptr> struct need_dma {
	need_dma() { enableDMAClock<ptr>(); }
};

} // namespace hal
