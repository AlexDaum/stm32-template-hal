#pragma once

#include "Util.h"
#include "dma.h"
#include "dma_ringbuffer.h"
#include "ringbuffer.h"
#include "stm.h"
#include <array>
#include <cmath>
#include <tuple>
#include <vector>

namespace hal {

enum Direction { SEND, RECEIVE };
enum UART_Parity { NONE = 0b00, EVEN = 0b10, ODD = 0b11 };
enum ReceiveResult { SUCCESS = 0, TOO_LONG, NO_DATA };

template <uintptr_t uart> struct UART_Acc {
	static uint8_t get();
	static void put(uint8_t c);
};

#define USART_CONSTEXPR_DMA_PTR                                                \
	template <uintptr_t base, Direction direction> constexpr uintptr_t

USART_CONSTEXPR_DMA_PTR getDMAForSerial() { return 0; }
USART_CONSTEXPR_DMA_PTR getDMAChannelForSerial() { return 0; }
USART_CONSTEXPR_DMA_PTR getDMAChannelNumberForSerial() { return 0; }

template <uintptr_t uart, uintptr_t dma_channel, Direction dir>
void confUART_DMA_CPAR();

template <uintptr_t ptr> constexpr DMA_TypeDef *castDMA() {
	return reinterpret_cast<DMA_TypeDef *>(ptr);
}

template <uintptr_t uart> void enableIRQs();

template <uintptr_t uart> void handleORE() {
	USART_TypeDef *usart = reinterpret_cast<USART_TypeDef*>(uart);
	usart->ICR = USART_ICR_ORECF;
}

template <uintptr_t ptr> constexpr DMA_Channel_TypeDef *castDMACh() {
	return reinterpret_cast<DMA_Channel_TypeDef *>(ptr);
}

template <uintptr_t base, size_t sendSize, size_t recvSize, bool dma,
		  u32 rcc_reg>
void confSerial(u32 baud, UART_Parity parity);

template <uintptr_t base, size_t sendSize, size_t recvSize, bool dma>
class USART_Accessor {};

template <uintptr_t uart> void setIRQ(void (*irq)());


// IRQ Accessor
template <uintptr_t base, size_t sendSize, size_t recvSize>
class USART_Accessor<base, sendSize, recvSize, false> {
  private:
	using this_t = USART_Accessor<base, sendSize, recvSize, false>;
	using acc = UART_Acc<base>;

	RingBuffer<uint8_t, sendSize> sendBuffer;
	RingBuffer<uint8_t, recvSize> recvBuffer;

	static this_t *backref;

	static constexpr USART_TypeDef *usart() {
		return reinterpret_cast<USART_TypeDef *>(base);
	}

	static void irq() {
		if (backref == nullptr)
			return;
		backref->irq_handler();
	}

	void irq_handler() {
		if (usart()->ISR & USART_ISR_RXNE) {
			if (!recvBuffer.isFull()) {
				recvBuffer.put(acc::get());
			}
		}
		if (usart()->ISR & USART_ISR_TXE) {
			if (sendBuffer.isNotEmpty()) {
				acc::put(sendBuffer.get());
			} else {
				usart()->CR1 &= ~USART_CR1_TXEIE;
				usart()->ISR &= USART_ISR_TXE;
			}
		}
		if (usart()->ISR & USART_ISR_ORE) {
			// Overrun
			handleORE<base>();
		}
		if(usart()->ISR & USART_ISR_FE){
			usart()->ICR = USART_ICR_FECF;
		}
	}

  public:
	USART_Accessor() {
		backref = this;
		enableIRQs<base>();
		setIRQ<base>(&irq);
	}

	~USART_Accessor() { backref = nullptr; }

	void put(uint8_t c) {
		sendBuffer.put(c);
		flush();
	}
	uint8_t get() { return recvBuffer.get(); }
	void flush() {
		reinterpret_cast<USART_TypeDef *>(base)->CR1 |=
			USART_CR1_TXEIE; // hope this is portable
	}

	bool isSendBufferFull() { return sendBuffer.isFull(); }
	bool isRecvBufferNotEmpty() { return recvBuffer.isNotEmpty(); }
	void wait() {
		while (sendBuffer.isNotEmpty())
			;
	}
};

template <uintptr_t base, size_t sendSize, size_t recvSize>
USART_Accessor<base, sendSize, recvSize, false>
	*USART_Accessor<base, sendSize, recvSize, false>::backref = nullptr;

// DMA Accessor
template <uintptr_t base, size_t sendSize, size_t recvSize>
class USART_Accessor<base, sendSize, recvSize, true> {
  private:
	bool startedDMA = false;
	static constexpr size_t sendBufSize() {
		return sendSize == 1 ? 1 : sendSize / 2;
	}
	using double_buffer = std::array<std::array<uint8_t, sendBufSize()>, 2>;

	// Will initialize the dma
	need_dma<getDMAForSerial<base, SEND>()> send_dma;
	need_dma<getDMAForSerial<base, SEND>()> recv_dma;

	// variables
	DMARingBuffer<uint8_t, getDMAChannelForSerial<base, RECEIVE>(), recvSize>
		recvBuffer;
	double_buffer sendBuffer;
	struct {
		u32 usedBuffer : 1;
		u32 idx : 31;
	};

	// methods
	u32 dmaFinish() {
		if (!startedDMA)
			return true;

		u32 dma_num = getDMAChannelNumberForSerial<base, SEND>();

		u32 dma_finish_mask = DMA_ISR_TCIF1 << ((dma_num - 1) * 4);
		DMA_TypeDef *dma = castDMA<getDMAForSerial<base, SEND>()>();
		return dma->ISR & dma_finish_mask;
	}

  public:
	USART_Accessor() : usedBuffer(0), idx(0) {
		static_assert(getDMAChannelForSerial<base, SEND>() != 0,
					  "Invalid DMA Channel for this Serial!");
		static_assert(getDMAChannelForSerial<base, RECEIVE>() != 0,
					  "Invalid DMA Channel for this Serial!");
		static_assert(getDMAForSerial<base, SEND>() != 0,
					  "Invalid DMA for this Serial!");
		static_assert(getDMAForSerial<base, RECEIVE>() != 0,
					  "Invalid DMA for this Serial!");
		static_assert(getDMAChannelNumberForSerial<base, SEND>() != 0,
					  "Invalid DMA Channel Number for this Serial!");
		static_assert(getDMAChannelNumberForSerial<base, RECEIVE>() != 0,
					  "Invalid DMA Channel Number for this Serial!");

		const uintptr_t dma_send_ptr = getDMAChannelForSerial<base, SEND>();
		const uintptr_t dma_read_ptr = getDMAChannelForSerial<base, RECEIVE>();
		DMA_Channel_TypeDef *const dma = castDMACh<dma_send_ptr>();
		dma->CCR = 0;
		confUART_DMA_CPAR<base, dma_send_ptr, SEND>();
		confUART_DMA_CPAR<base, dma_read_ptr, RECEIVE>();
		recvBuffer.startDMA();
		dma->CCR = DMA_CCR_PSIZE_0 | DMA_CCR_MINC | DMA_CCR_DIR;
	}

	void put(uint8_t c) {
		sendBuffer[usedBuffer][idx] = c;
		idx++;
		if (idx >= sendBufSize()) {
			flush();
		}
	}
	uint8_t get() { return recvBuffer.get(); }

	void flush() {
		while (!dmaFinish())
			;
		DMA_TypeDef *dmat = castDMA<getDMAForSerial<base, SEND>()>();
		dmat->IFCR = DMA_IFCR_CTCIF1
					 << ((getDMAChannelNumberForSerial<base, SEND>() - 1) *
						 4); // clear finish flag
		if (idx == 0) {
			startedDMA = false;
			return;
		}
		DMA_Channel_TypeDef *dma =
			castDMACh<getDMAChannelForSerial<base, SEND>()>();
		dma->CCR &= ~DMA_CCR_EN;
		dma->CNDTR = idx;
		dma->CMAR = (uintptr_t)(&sendBuffer[usedBuffer][0]);
		USART_TypeDef *usart = reinterpret_cast<USART_TypeDef *>(base);
		usart->ICR = USART_ICR_TCCF;
		dma->CCR |= DMA_CCR_EN;
		//		USART_TypeDef *usart = reinterpret_cast<USART_TypeDef *>(base);
		//		usart->TDR = sendBuffer[usedBuffer][0];
		startedDMA = true;
		idx = 0;
		usedBuffer = usedBuffer ? 0 : 1;
	}

	void wait() {
		while (!dmaFinish())
			;
	}

	bool isSendBufferFull() { return idx >= sendBufSize(); }
	bool isRecvBufferNotEmpty() { return recvBuffer.hasData(); }
};

/*========================
 * SERIAL starts here
 *========================*/

template <uintptr_t base, uintptr_t rcc_reg, uintptr_t rcc_mask,
		  size_t sendSize, size_t recvSize, bool dma>
class Serial {
  private:
	// typedefs
	using accessor_t = USART_Accessor<base, sendSize, recvSize, dma>;

	// variables
	accessor_t accessor;

	// methods

  public:
	Serial(int baud = 9600, UART_Parity parity = NONE) {
		enable();
		confSerial<base, sendSize, recvSize, dma, rcc_reg>(baud, parity);
	}
	~Serial() {}

	// Configuration
	void enable() { *reinterpret_cast<volatile u32 *>(rcc_reg) |= rcc_mask; }
	void disable() { *reinterpret_cast<volatile u32 *>(rcc_reg) &= ~rcc_mask; }

	// Serial transmit
	void send(uint8_t c) { accessor.put(c); }
	void print(const char *s) {
		for (; *s; s++)
			send(*s);
	}

	void print(const std::vector<u8> &vect){
		for(u8 c : vect){
			send(c);
		}
	}

	template <typename t, t radix = 10, typename = isInt<t>> void print(t i) {
		static_assert(radix <= 16, "Radix may be at most 16");
		if (i == 0) {
			send('0');
			return;
		}
		const size_t max =
			(size_t)(sizeof(t) * 8 * std::log(2) / std::log(radix) + 2);
		std::array<char, max> buf;
		int len = max - 1;
		buf[len] = '\0';
		while (i) {
			t rem = i % radix;
			i /= radix;
			buf[--len] = util::intToHex(rem);
		}
		print((const char *)&buf[len]);
	}

	template <typename t, t radix = 10, size_t len, typename = isInt<t>>
	void printFixLen(t i) {
		char buf[len + 1];
		std::fill_n(buf, len, '0');
		buf[len] = '\0';
		size_t idx = len;
		while (i) {
			t rem = i % radix;
			i /= radix;
			buf[--idx] = util::intToHex(rem);
		}
		print(buf);
	}

	template <size_t len>
	std::tuple<ReceiveResult, size_t> get_n(std::array<u8, len> &arr,
											u8 terminator = '\n') {
		size_t i = 0;
		do {
			if (!accessor.isRecvBufferNotEmpty()) {
				arr[i] = '\0';
				std::make_tuple(NO_DATA, i + 1);
			}
			arr[i] = get();
		} while (arr[i++] != terminator && i < len - 1);
		ReceiveResult res = arr[i - 1] == terminator ? SUCCESS : TOO_LONG;
		arr[i] = '\0';
		return std::make_tuple(res, i + 1);
	}

	template <size_t len>
	std::tuple<ReceiveResult, size_t> get_n_blocking(std::array<u8, len> &arr,
													 u8 terminator = '\n') {
		size_t i = 0;
		u32 timeout = 1000000;
		do {
			while (!accessor.isRecvBufferNotEmpty() && timeout)
				timeout--;
			if (!timeout) {
				arr[i] = '\0';
				std::make_tuple(NO_DATA, i + 1);
			}
			timeout = 1000000;
			arr[i] = get();
		} while (arr[i++] != terminator && i < len);
		ReceiveResult res = arr[i - 1] == terminator ? SUCCESS : TOO_LONG;
		arr[i] = '\0';
		return std::make_tuple(res, i + 1);
	}

	bool available() { return accessor.isRecvBufferNotEmpty(); }

	uint8_t get() { return accessor.get(); }

	void flush() { accessor.flush(); }
	void wait() { accessor.wait(); }

#ifdef USART_CR3_DEM
	void enableDE(){
		USART_TypeDef *usart = reinterpret_cast<USART_TypeDef*>(base);
		usart->CR1 &= ~USART_CR1_UE;
		usart->CR3 |= USART_CR3_DEM;
		usart->CR1 |= USART_CR1_DEAT_2 | USART_CR1_DEDT_2;
		usart->CR1 |= USART_CR1_UE;
	}
#endif

};

template <typename t> struct is_serial : std::false_type {};

template <uintptr_t base, uintptr_t rcc_reg, uintptr_t rcc_mask,
		  size_t sendSize, size_t recvSize, bool dma>
struct is_serial<Serial<base, rcc_reg, rcc_mask, sendSize, recvSize, dma>>
	: std::true_type {};

template <typename t> inline constexpr bool is_serial_v = is_serial<t>::value;

template <typename t> using is_serial_t = std::enable_if_t<is_serial_v<t>>;

} // namespace hal

#include "variants/uart_variants.h"
