#pragma once

#include <array>
#include <cmath>
#include "haldefs.h"
#include <string.h>

namespace util {
u8 hexToInt(u8 c);
u8 intToHex(u8 i);

template <typename t, t radix = 10, typename = isInt<t>>
int snprint(t i, char *buffer, size_t len) {
  static_assert(radix <= 16, "Radix may be at most 16");
  if (len == 0) return -1;
  if (i == 0) {
    buffer[0] = '0';
    return - 1;
  }
  const size_t max =
      (size_t)(sizeof(t) * 8 * std::log(2) / std::log(radix) + 2);
  std::array<char, max> buf;
  if (len < max) return -1;
  int last = max - 1;
  buf[last] = '\0';
  while (i) {
    t rem = i % radix;
    i /= radix;
    buf[--last] = util::intToHex(rem);
  }
  if(max - last > len)
	  return -1;
  memcpy(buffer, &buf[last], max - last);
  return (max - len);
}
}  // namespace util
