/*
 * systick.h
 *
 *  Created on: 15.11.2018
 *      Author: alex
 */

#ifndef SYSTICK_H_
#define SYSTICK_H_

#include <stdint.h>
#include <timebase.h>

extern "C" uint32_t SystemCoreClock;

namespace sTick {

int init(hal::timebase_t tbase = hal::MILLIS, int ticks = 1);
void delay(uint32_t delay);
uint32_t getTime();
void resetTime();

} // namespace sTick

#endif /* SYSTICK_H_ */
