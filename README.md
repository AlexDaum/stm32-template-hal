# Usage
To use this Library, simple add the src folder to the source path in your IDE and
the inc folder to the include path

This currently needs `std=c++17` since I used some features of it and it is
available, so why don't use it

# Currently Supported Chips
Currently only the stm32f030f4 and stm32f303re are supported, but more chips can
be added to the library.

Right now there is also a limited number of interfaces supported, only UART,
ADC, Timer, GPIO and SysTick

# How it is designed
All Peripherals, except the SysTick, have their own class. That class is a class
template, depending on the base register, the rcc register and rcc mask and
for some peripherals additional parameters.

The Part Specific Code parts are defined in seperate header Files, that are
located in their own subfolder in the inc folder and included by the
*peripheral*_variants.h File depending on defines of the part number
