#include "serial.h"

#ifdef SERIAL1
void (*SERIAL1_Handler)();
extern "C" void SERIAL1() {
	if (SERIAL1_Handler)
		SERIAL1_Handler();
}
#endif

#ifdef SERIAL2
void (*SERIAL2_Handler)();
extern "C" void SERIAL2() {
	if (SERIAL2_Handler)
		SERIAL2_Handler();
}
#endif

#ifdef SERIAL3
void (*SERIAL3_Handler)();
extern "C" void SERIAL3() {
	if (SERIAL3_Handler)
		SERIAL3_Handler();
}
#endif

#ifdef SERIAL4
void (*SERIAL4_Handler)();
extern "C" void SERIAL4() {
	if (SERIAL4_Handler)
		SERIAL4_Handler();
}
#endif

#ifdef SERIAL5
void (*SERIAL5_Handler)();
extern "C" void SERIAL5() {
	if (SERIAL5_Handler)
		SERIAL5_Handler();
}
#endif

