#include "systick.h"
#include "stm.h"

volatile uint32_t tick;
volatile uint32_t time;

int sTick::init(hal::timebase_t tbase, int ticks) {
	int retCode = SysTick_Config(ticks * (SystemCoreClock / tbase));
	time = 0;
	if (SysTick_IRQn > 0) // IRQs with < 0 are always active
		NVIC_EnableIRQ(SysTick_IRQn);
	return retCode;
}
void sTick::delay(uint32_t delay) {
	tick = 0;
	while (tick < delay)
		__WFI();
}

uint32_t sTick::getTime() { return time; }
void sTick::resetTime() { time = 0; }

extern "C" void SysTick_Handler() {
	tick++;
	time++;
}
